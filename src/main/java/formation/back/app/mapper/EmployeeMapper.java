package formation.back.app.mapper;

import org.mapstruct.Mapper;
import formation.back.app.dto.EmployeeDto;
import formation.back.app.entities.EmployeeEntity;

@Mapper(componentModel = "spring")
public interface EmployeeMapper extends EntityMapper<EmployeeDto, EmployeeEntity> {

}
