package formation.back.app.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import formation.back.app.dto.EmployeeDto;
import formation.back.app.dto.EmployeeDto.EmployeeDtoBuilder;
import formation.back.app.entities.EmployeeEntity;
import formation.back.app.entities.EmployeeEntity.EmployeeEntityBuilder;

@Component
public class PersonMapperImpl implements EmployeeMapper {

	@Override
	public EmployeeEntity toEntity(EmployeeDto dto) {
		if (dto == null) {
			return null;
		}
		EmployeeEntityBuilder employeeEntity = EmployeeEntity.builder();

		employeeEntity.firstName(dto.getFirstName());
		employeeEntity.lastName(dto.getLastName());
		employeeEntity.job(dto.getJob());
		employeeEntity.email(dto.getEmail());
		employeeEntity.idEmployee(dto.getIdEmployee());

		return employeeEntity.build();
	}

	@Override
	public EmployeeDto toDto(EmployeeEntity entity) {
		if (entity == null) {
			return null;
		}

		EmployeeDtoBuilder employeeDto = EmployeeDto.builder();

		employeeDto.firstName(entity.getFirstName());
		employeeDto.lastName(entity.getLastName());
		employeeDto.job(entity.getJob());
		employeeDto.email(entity.getEmail());
		employeeDto.idEmployee(entity.getIdEmployee());

		return employeeDto.build();
	}

	@Override
	public void updateEntityFromDto(EmployeeDto dto, EmployeeEntity entity) {

		if (dto == null) {
			return;
		}
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setJob(dto.getJob());

	}

	@Override
	public List<EmployeeEntity> toEntity(List<EmployeeDto> dtoList) {
		 if ( dtoList == null ) {
	            return null;
	        }

	        List<EmployeeEntity> list = new ArrayList<EmployeeEntity>( dtoList.size() );
	        for ( EmployeeDto employeeDto : dtoList ) {
	            list.add( toEntity( employeeDto ) );
	        }

	        return list;
	}

	@Override
	public List<EmployeeDto> toDto(List<EmployeeEntity> entityList) {
		if ( entityList == null ) {
            return null;
        }

        List<EmployeeDto> list = new ArrayList<EmployeeDto>( entityList.size() );
        for ( EmployeeEntity employeeEntity : entityList ) {
            list.add( toDto( employeeEntity ) );
        }

        return list;
	}

}
