package formation.back.app.mapper;

import java.util.List;

import org.mapstruct.MappingTarget;

public interface EntityMapper<D, E> {

	E toEntity(D dto);

	D toDto(E entity);
	
	List<E> toEntity(List<D> dtoList);
	
    List <D> toDto(List<E> entityList);

	void updateEntityFromDto(D dto, @MappingTarget E entity);

}
