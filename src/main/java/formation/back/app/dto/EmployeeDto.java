package formation.back.app.dto;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * A DTO for the Employee entity.
 */
@Getter
@Setter
@Builder
public class EmployeeDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/***public id that i can share***/
	private String idEmployee;

	@NotNull
	@Size(min = 3, max = 20)
	private String firstName;

	@NotNull
	@Size(min = 3, max = 20)
	private String lastName;

	@NotNull
	@Size(min = 2, max = 50)
	private String job;

	@NotNull
	@Size(min = 2, max = 50)
	private String email;

	private String createdBy;
	private Date createdAt;
	private String updatedBy;
	private Date updatedAt;
	private Date deletedAt;

}
