package formation.back.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import formation.back.app.entities.common.AbstractTracableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "employee")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeEntity extends AbstractTracableEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6344346573294837885L;

	@Column(name = "idEmployee", nullable = false)
	private String idEmployee;

	@Column(nullable = false, length = 120, unique = true)
	private String email;

	@Column(name = "firstName", nullable = false, length = 50)
	private String firstName;

	@Column(name = "lastName", nullable = false, length = 50)
	private String lastName;

	@Column(name = "job", nullable = false, length = 80)
	private String job;

}
