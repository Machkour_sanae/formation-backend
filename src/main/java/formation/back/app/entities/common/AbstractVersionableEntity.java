package formation.back.app.entities.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class AbstractVersionableEntity extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5763827745308343856L;

	@Version
	@Column(name = "version", nullable = false)
	private Long version;

}
