package formation.back.app.entities.common;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class AbstractTracableEntity extends AbstractVersionableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5763827745308343856L;

	@Column(name = "created_by", nullable = false)
	private String createdBy;

	@Column(name = "created_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Column(name = "update_by")
	private String updatedBy;

	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;

	@Column(name = "deleted_by")
	private String deletedBy;

	@Column(name = "deleted_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedAt;

	@PrePersist
	protected void initCreationTrace() {
		createdAt = new Date();
		createdBy = "anonyme";
	}

	@PreUpdate
	protected void initUpdateTrace() {
		updatedAt = new Date();
		updatedBy = "anonyme";
	}

}
