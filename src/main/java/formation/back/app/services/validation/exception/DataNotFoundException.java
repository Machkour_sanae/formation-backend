package formation.back.app.services.validation.exception;

public class DataNotFoundException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3803282516485587979L;

	public DataNotFoundException(String message, Object... args) {
		super(404, "data.not.found", message, args);
	}

	public DataNotFoundException() {
		this("data.not.found");
	}
}
