package formation.back.app.services;

import java.util.List;

import formation.back.app.dto.EmployeeDto;


/**
 * A Service for the Employee entity.
 */
public interface EmployeeService {
	
	EmployeeDto createEmployee(EmployeeDto employeeDto);
	
	EmployeeDto getEmployeeById(String idEmployee);
	
	void deleteEmployee(String idEmployee);

	EmployeeDto updateEmployee(String idEmployee , EmployeeDto employeeDto);
	
	List<EmployeeDto> getListEmployeeByFirstName(String firstName);
	
	List<EmployeeDto> sortListEmployeeByFirstName();
	

}
