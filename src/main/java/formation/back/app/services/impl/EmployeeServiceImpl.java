package formation.back.app.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import formation.back.app.shared.Utils;
import formation.back.app.dao.EmployeeDao;
import formation.back.app.dto.EmployeeDto;
import formation.back.app.entities.EmployeeEntity;
import formation.back.app.mapper.EmployeeMapper;
import formation.back.app.services.EmployeeService;
import formation.back.app.services.validation.exception.DataNotFoundException;

/**
 * Service Implementation for managing the Employee.
 */
@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	@Autowired
	private EmployeeMapper employeeMapper;

	@Autowired
	private Utils util;

	@Override
	public EmployeeDto createEmployee(EmployeeDto employee) {
		EmployeeEntity employeeEntity = employeeMapper.toEntity(employee);
		employeeEntity.setIdEmployee(util.generateEmployeeID(32));
		EmployeeEntity user = employeeDao.create(employeeEntity);
		return employeeMapper.toDto(user);

	}

	@Override
	public EmployeeDto getEmployeeById(String idEmployee) {
		EmployeeEntity employeeEntity = employeeDao.findByIdEmployee(idEmployee)
				.orElseThrow(DataNotFoundException::new);
		return employeeMapper.toDto(employeeEntity);
	}

	@Override
	public void deleteEmployee(String idEmployee) {
		EmployeeEntity employeeEntity = employeeDao.findByIdEmployee(idEmployee)
				.orElseThrow(DataNotFoundException::new);
		employeeDao.delete(employeeEntity);
	}

	@Override
	public EmployeeDto updateEmployee(String idEmployee, EmployeeDto employeeDto) {
		EmployeeEntity employeeEntity = employeeDao.findByIdEmployee(idEmployee)
				.orElseThrow(DataNotFoundException::new);
		employeeMapper.updateEntityFromDto(employeeDto, employeeEntity);
		EmployeeEntity user = employeeDao.update(employeeEntity);
		return employeeMapper.toDto(user);
	}

	@Override
	public List<EmployeeDto> getListEmployeeByFirstName(String firstName) {
		List<EmployeeEntity> employees = employeeDao.findAllEmployeesByFirstName(firstName);

		return employeeMapper.toDto(employees);
	}

	@Override
	public List<EmployeeDto> sortListEmployeeByFirstName() {
		List<EmployeeEntity> employees = employeeDao.sortListEmployeesByFirstName();

		return employeeMapper.toDto(employees);
	}

}
