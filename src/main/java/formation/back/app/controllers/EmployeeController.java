package formation.back.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import formation.back.app.dto.EmployeeDto;
import formation.back.app.services.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("/{idEmployee}")
	public ResponseEntity<EmployeeDto> getEmployee(@PathVariable String idEmployee) {
		return new ResponseEntity<EmployeeDto>(employeeService.getEmployeeById(idEmployee), HttpStatus.OK);

	}
	
	@GetMapping
	public ResponseEntity<List<EmployeeDto>> getListEmployeeByFirstName(@RequestParam(value="firstName") String firstName) {
		
		return new ResponseEntity<List<EmployeeDto>>(employeeService.getListEmployeeByFirstName(firstName), HttpStatus.OK);

	}
	
	/* sorted by first Name */
	@GetMapping("/sorted")
	public ResponseEntity<List<EmployeeDto>> sortListEmployeeByFirstName() {
		
		return new ResponseEntity<List<EmployeeDto>>(employeeService.sortListEmployeeByFirstName(), HttpStatus.OK);

	}

	@PutMapping("/{idEmployee}")
	public ResponseEntity<EmployeeDto> updateEmployee(@PathVariable String idEmployee,
			@RequestBody EmployeeDto employee) {
		
		return new ResponseEntity<EmployeeDto>(employeeService.updateEmployee(idEmployee, employee),
				HttpStatus.ACCEPTED);

	}

	@PostMapping
	public ResponseEntity<EmployeeDto> createEmployee(@RequestBody EmployeeDto employee) {
		return ResponseEntity.status(HttpStatus.CREATED).body(employeeService.createEmployee(employee));
	}

	@DeleteMapping("/{idEmployee}")
	public ResponseEntity<Void> deleteEmployee(@PathVariable String idEmployee) {
		employeeService.deleteEmployee(idEmployee);
		return ResponseEntity.noContent().build();

	}
}
