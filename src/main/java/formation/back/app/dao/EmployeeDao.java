package formation.back.app.dao;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import formation.back.app.entities.QEmployeeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.QSort;
import org.springframework.stereotype.Repository;
import org.apache.commons.lang3.StringUtils;
import com.querydsl.jpa.impl.JPAQuery;

import formation.back.app.dao.common.AbstractDao;
import formation.back.app.entities.EmployeeEntity;

@Repository
public class EmployeeDao extends AbstractDao<EmployeeEntity, EmployeeRepository> {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public EmployeeRepository getJpaRepository() {
		return employeeRepository;
	}

	public Optional<EmployeeEntity> findByIdEmployee(String idEmployee) {
		return employeeRepository.findByIdEmployee(idEmployee);
	}
	
	
	public List<EmployeeEntity> findAllEmployeesByFirstName(String firstName){
		JPAQuery<EmployeeEntity> query = getJPAQueryFactory().selectFrom(QEmployeeEntity.employeeEntity);
        if(StringUtils.isNotBlank(firstName)) {
            query.where(QEmployeeEntity.employeeEntity.firstName.eq(firstName));
        }
       
        return (List<EmployeeEntity>) query.fetch();
		
		
	}
	
	public List<EmployeeEntity> sortListEmployeesByFirstName(){
	
		 return employeeRepository.findAll(Sort.by("firstName")); 
	
	}

}
